package com.rpm.ref.search;

import java.util.Arrays;
import java.util.Scanner;

/**
 * класс для реализации нахождения числа в массиве
 *
 * @Author Mashina
 */
public class NumberSearch {
    private static Scanner scanner = new Scanner(System.in);
    private static final int MIN_ARR_VALUE = -25;
    private static final int RANGE_IN_ARRAY = 51;
    public static void main(String[] args) {

        System.out.print("What size array be must: ");
        int arraySize = inputArraySize();

        int [] randomArr = arrayRandomize(arraySize);
        outArray(randomArr);

        System.out.print("Input finding number: ");
        int number = scanner.nextInt();

        outCheckResult(randomArr, number);
    }

    /**
     * принимает от пользователя значение размерности массива
     * проверяет корректность введеннях данных 1<n<100;
     *
     * @return корректный размер массива
     */
    private static int inputArraySize() {
        int size;
        do {
            size = scanner.nextInt();
        } while (size < 1 || size > 100);
        return size;
    }

    /**
     * рандомно заполняет элементы массива
     * в диапазне от MIN_ARR_VALUE в диапазоне RANGE_IN_ARRAY
     * заданного размера
     *
     * @param arraySize размер массива
     * @return рандомный массив элементов
     */
    private static int[] arrayRandomize(int arraySize) {
        int[] randomMass = new int[arraySize];
        for (int count = 0; count < arraySize; count++) {
            randomMass[count] = MIN_ARR_VALUE + (int) (Math.random() * RANGE_IN_ARRAY);
        }
        return randomMass;
    }

    /**
     * проверяет наличие искомого элемента в массиве
     *
     * @param massive массив элементов
     * @param number  искомый элемент
     * @return индекс элемента или значение его отсутвия (-1)
     */
    private static int checkNumInArray(int[] massive,int number) {
        int findNum = -1;
        for (int count = 0; count < 5; count++) {
            if (massive[count] == number) {
                return count;
            }
        }
        return findNum;
    }

    /**
     * выводит индекс найденного элемента
     * либо сообщает об его отсутвии
     *
     * @param massive массив элементов
     * @param number  искомый элемент
     */
    private static void outCheckResult(int[] massive, int number) {
        int findNum = checkNumInArray(massive, number);
        if (findNum == -1) {
            System.out.println("Not found number in massive");
        } else {
            System.out.println("Index of number in massive: " + findNum);
        }
    }

    /**
     * выводит элементы массива
     *
     * @param massive массив элементов
     */
    private static void outArray(final int[] massive) {
        System.out.println(Arrays.toString(massive));
    }
}
